import time
import requests
from datetime import datetime
import pandas as pd


def get_data(strparam):
    # Get csv online
    url = 'https://opendata.bordeaux-metropole.fr/explore/dataset/st_park_p/download/?format=csv&timezone=Europe/Paris&lang=fr&use_labels_for_header=true&csv_separator=%3B'
    r = requests.get(url, allow_redirects=True)
    # To be discussed : Save one copy or more ?????
    open('raw_data/bordeaux_parking_'+strparam+'.csv', 'wb').write(r.content)
    # time.sleep(1)
    print('get data at'+strparam)


def process(strparam):
    raw_data = pd.read_csv('raw_data/bordeaux_parking_'+strparam+'.csv', sep=';')
    raw_data_subset = raw_data[['Geo Point', 'insee', 'nom', 'propr', 'adresse', 'etat', 'libres',
                                'np_total', 'np_pr', 'th_quar', 'th_demi', 'th_heur', 'secteur', 'connecte', 'mdate']]
    raw_data_subset.to_csv('preprocessed_data/bordeaux_parking_processed'+strparam+'.csv')
    print('data preprocessed at'+strparam)



if __name__ == "__main__":
    while True:
        str_time = (datetime.now().strftime("%Y%m%d-%H%M%S"))
        get_data(str_time)
        process(str_time)
        time.sleep(150)  # do work every one hour
